---
layout: inner
title: About
permalink: /about/
---
# About


<!-- [Link to another page](/index.html). -->

There should be whitespace between paragraphs.

## Paragraph

This is a normal paragraph following a header. GitHub is a code hosting platform for version control and collaboration. It lets you and others work together on projects from anywhere.

## Blockquotes

> This is a blockquote following a header.
>
> When something is important enough, you do it even if the odds are not in your favor.

